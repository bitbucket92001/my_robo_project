#ifndef _ARM_H
#define _ARM_H

#include "ecrobot_interface.h"
#include "constant.h"
#include "nxt_init.h"

extern void arm_open(void);
extern void arm_close(void);

#endif
