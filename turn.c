#include "turn.h"


void turn(int read_mode){
	static float op_pd;
	count_reset(0,0);
		while(nxt_motor_get_count(L_MOTOR) < (int)(C1 * DISTANCE - 15)){	//車幅を半径とする円周の4分の1
		nxt_motor_set_speed(R_MOTOR,-50,1);	//-50
		nxt_motor_set_speed(L_MOTOR,50,1);	//50
	}
	count_reset(0,0);
	while(nxt_motor_get_count(R_MOTOR) < C1 * 33){		//ボールを取る位置を調整
		op_pd = read_left();
		run_left_sensor(38,op_pd);
	}
	stop(10);
	count_reset(0,0);
	while(nxt_motor_get_count(R_MOTOR) < C1 * 4){		//前に出ながらアームを閉じる
		op_pd = read_left();
		run_left_sensor(SPEED,op_pd);
		nxt_motor_set_speed(ARM_MOTOR,-40,1);
	}
	count_reset(10,10);		//戻すための値
	while(nxt_motor_get_count(R_MOTOR) > 0){
		nxt_motor_set_speed(L_MOTOR,-81,1);
		nxt_motor_set_speed(R_MOTOR,-80,1);
	}
	stop(10);
	count_reset(0,0);
	while(nxt_motor_get_count(R_MOTOR) < C1 * DISTANCE * 2 - 25 ){
		nxt_motor_set_speed(R_MOTOR,50,1);
		nxt_motor_set_speed(L_MOTOR,-50,1);
		systick_wait_ms(3);
	}
}


void turn_left(int after_turn_read_mode){
	count_reset(0,0);
	// 次は左右どちらのセンサーでラインを読むかで処理を決める
	//	*0: 右でラインを読み 左で交差点を検知
	//	*1: 左でラインを読み 右で交差点を検知
	switch (after_turn_read_mode){
		case 0:
			while(nxt_motor_get_count(R_MOTOR) < (C1 * DISTANCE  + 25)){	//車幅を直径とする円周の4分の1
				nxt_motor_set_speed(L_MOTOR,-50,1);	//-50
				nxt_motor_set_speed(R_MOTOR,50,1);	//50
				systick_wait_ms(3);
			}
			break;

		case 1:
			while(nxt_motor_get_count(R_MOTOR) < (C1 * DISTANCE - 20)){	//車幅を半径とする円周の4分の1
				nxt_motor_set_speed(L_MOTOR,-50,1);	//-50
				nxt_motor_set_speed(R_MOTOR,50,1);	//50
				systick_wait_ms(3);
			}
			break;
	}
	return_line(after_turn_read_mode);
}


void turn_right(int after_turn_read_mode){
	count_reset(0,0);
	// 次は左右どちらのセンサーでラインを読むかで処理を決める
	//	*0: 右でラインを読み 左で交差点を検知
	//	*1: 左でラインを読み 右で交差点を検知
	switch (after_turn_read_mode){
		case 0:
			while(nxt_motor_get_count(L_MOTOR) < (int)(C1 * DISTANCE - 10)){	//車幅を直径とする円周の4分の1
				nxt_motor_set_speed(L_MOTOR,50,1);	//51
				nxt_motor_set_speed(R_MOTOR,-50,1);	//-50
				systick_wait_ms(3);
			}
			break;

		case 1:
			while(nxt_motor_get_count(L_MOTOR) < (int)(C1 * DISTANCE + 5)){	//車幅を半径とする円周の4分の1	&& ecrobot_get_light_sensor(R_LIGHT_SENSOR) < GRAY_VAL)*/
				nxt_motor_set_speed(L_MOTOR,50,1);	//50
				nxt_motor_set_speed(R_MOTOR,-50,1);	//-50
				systick_wait_ms(3);
			}
			break;
	}
	count_reset(0,0);
	while(nxt_motor_get_count(R_MOTOR) < C1 * 2){
		nxt_motor_set_speed(L_MOTOR,46,1);	//46
		nxt_motor_set_speed(R_MOTOR,45,1);	//45
		systick_wait_ms(3);
	}
	return_line(after_turn_read_mode);
}
