#include "arm.h"

void arm_open(){
	nxt_motor_set_count(ARM_MOTOR,0);
	while( nxt_motor_get_count(ARM_MOTOR) <= 30 ){
		nxt_motor_set_speed(ARM_MOTOR,40,1);
		systick_wait_ms(4);
	}
}

void arm_close(){
	while( nxt_motor_get_count(ARM_MOTOR) > 0  ){
		nxt_motor_set_speed(ARM_MOTOR, -40,1);
		systick_wait_ms(4);
	}
}
