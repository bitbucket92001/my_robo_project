#ifndef _MUSIC_H
#define _MUSIC_H

#include "ecrobot_interface.h"

#define TEMPO  40
#define REST4 TEMPO * 4  + 10

#define TEMPO_NOTME  40
#define REST_NOTME TEMPO_NOTME * 4 + 10
#define NOTEME_LENGTH 122

#define TEMPO_RPG 45
#define REST_RPG  TEMPO_RPG * 4 + 10 //４分休符
#define RPG_LENGTH 46

#define TEMPO_TOTTOKO 25
#define REST_TOTTOKO TEMPO_TOTTOKO * 4 + 10 //４分休符
#define TOTTOKO_LENGTH 52

#define TEMPO_IMP 50
#define REST_IMP TEMPO_IMP * 4 + 10 //４分休符
#define IMP_LENGTH 200

#define REST 0 // 休符フラグ
#define VOLUME 20 //音量

#define DO3 130 //C3 middle
#define DO3S 138 //C3#
#define RE3 146 //D3
#define RE3S 155 //D3#
#define MI3 164 //E3
#define FA3 174 //F3
#define FA3S 185//F3#
#define SO3 196 //G3
#define SO3S 207 //G3#
#define RA3 220 //A3
#define RA3S 233 //A3#
#define SI3 247 //B4

#define DO4 261 //C4 middle
#define DO4S 277 //C4#
#define RE4 293 //D4
#define RE4S 311 //D4#
#define MI4 329 //E4
#define FA4 349 //F4
#define FA4S 367//F4#
#define SO4 392 //G4
#define SO4S 415 //G4#
#define RA4 440 //A4
#define RA4S 466 //A4#
#define SI4 494 //B4

#define DO5 523 //C5
#define DO5S 554 //C5#
#define RE5 587 //D5
#define RE5S 622 //D5#
#define MI5 659 //E5
#define FA5 698 //F5
#define FA5S 740 //F5#
#define SO5 784 //G5
#define SO5S 830 //G5#
#define RA5 880 //A5
#define RA5S 932 //A5#
#define SI5 987 //B5

#define DO6 1046 //C6
#define DO6S 1108 //C6#
#define RE6 1174 //D6
#define RE6S 1244 //D6#
#define MI6 1318 //E6
#define FA6 1396 //F6
#define FA6S 1480 //F6#
#define SO6 1568 //G6
#define SO6S 1661 //G6#
#define RA6 1760 //A6
#define RA6S 1864 //A6#
#define SI6 1975 //B6

#define DO7 2093 //C7
#define DO7S 2217 //C7#
#define RE7 2349 //D7
#define RE7S 2489 //D7#
#define MI7 2637 //E7
#define FA7 2793 //F7
#define FA7S 2956 //F7#
#define SO7 3136 //G7
#define SO7S 3322 //G7#
#define RA7 3520 //A7
#define RA7S 3729 //A7#
#define SI7 3951 //B7

extern short sheet_RPG[][2];
extern short sheet_tottoko[][2];
extern short sheet_notme[][2];
extern short sheet_imp[][2];
extern short sheet_Believe[][2];
extern void play_music(short [][2], short);

#endif
