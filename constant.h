#ifndef _CONST_H
#define _CONST_H

#include "ecrobot_interface.h"

/* 入出力ポートの定義 */
#define L_LIGHT_SENSOR NXT_PORT_S1
#define R_LIGHT_SENSOR NXT_PORT_S2
#define COLOR_SENSOR NXT_PORT_S3
#define SONAR NXT_PORT_S4
#define ARM_MOTOR NXT_PORT_A
#define L_MOTOR NXT_PORT_B
#define R_MOTOR NXT_PORT_C

#define GOSA 20
#define M_PI 3.14159265359
#define C1 13.8	//1cmに必要な回転角、正確には13.814
#define DIAMETER 8.3  //タイヤの直径(cm)
#define WIDTH 11.2  //車幅(cm)
#define DISTANCE WIDTH * M_PI  / 4  //車幅(cm)

//灰色を基準に
/**
*         ito  hiro
*黒: 690, 670, 600
*白: 510, 480, 380
*講堂     ito  hiro
*黒       680
*白       470
*第二前
*黒:      710 690
*白:      510 500
*/
#define BLACK_VAL 700
#define WHITE_VAL 500
#define GRAY_VAL (BLACK_VAL + WHITE_VAL) / 2
#define P_GAIN 0.48
#define D_GAIN 15
#define SPEED 90

extern int r_val, b_val;
extern S16 rgb[3];

#endif
