#ifndef _MYDEF_H
#define _MYDEF_H

#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
#include "constant.h"

void ecrobot_device_initialize(void);	/* OSEK起動時の処理 */
void ecrobot_device_terminate(void);		/* OSEK終了時の処理 */
void user_1ms_isr_type2(void);

#endif
