#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
#include "constant.h"
#include "nxt_init.h"
#include "control.h"
#include "operation.h"

static int point = 0;

//*****************************************************************************
// 0: 左旋回
// 1: 右旋回
// 2: 直進
// 3: 180旋回
// 4: ショートカット
// 5: グレーゾーン
// コース用   		3, 4, 5, 0, 0, 1, 1, 0, 0, 0
int curves[] = {3, 4, 5, 6, 0, 1, 1, 0, 0, 0, 0, 0};
//*****************************************************************************
//	*0: 右でラインを読み 左で交差点を検知
//	*1: 左でラインを読み 右で交差点を検知
//    								0, 1, 1, 0, 0, 1, 1, 0, 0, 1
static int read_mode[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//                                      o
//*****************************************************************************
int r_val=0, b_val=0;
S16 rgb[3];

DeclareTask(Task1);				/* Task1を宣言 */

TASK(Task1){
	float op_pd;
	int counter = 0;
	arm_open();
	ecrobot_set_nxtcolorsensor(COLOR_SENSOR, NXT_COLORSENSOR);//動作モード設定
	while(1){		//メインループ
				//右でラインを読む
				//左センサーが白を検知している間ループ
				while(GRAY_VAL >= ecrobot_get_light_sensor(L_LIGHT_SENSOR)){
					ecrobot_process_bg_nxtcolorsensor();
					op_pd = read_right();
					if(point == 2 && counter < 100){
						ecrobot_get_nxtcolorsensor_rgb(COLOR_SENSOR, rgb);
						r_val += rgb[0];
						b_val += rgb[2];
						counter++;
						display_update();
						systick_wait_ms(4);
					}
					run_right_sensor(80,op_pd);
				}
				decide_control(curves[point], read_mode[point+1]);
	point++;
	}		//メインループ終わり
	display_string("finish");		/* メッセージを表示する */
	display_update();
	TerminateTask();					/* 処理終了 */
}
