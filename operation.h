#ifndef _OP_H
#define _OP_H

#include "ecrobot_interface.h"
#include "constant.h"
#include "nxt_init.h"
#include "arm.h"
#include "control.h"
#include "music.h"
#include "turn.h"
#include "etc.h"

extern int decide_color();
extern void shortcut(void);
extern void decide_control(int, int);
extern void return_line(int);
extern void receive(int);
extern void doraGenai(int);

#endif
