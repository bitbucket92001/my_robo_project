#ifndef _CTL_H
#define _CTL_H

#include "ecrobot_interface.h"
#include "constant.h"
#include "nxt_init.h"

extern void run_right_sensor(int, int);
extern void run_left_sensor(int, int);
extern void run_back(int,int);
extern float read_right();
extern float read_left();
extern float read_gray();

#endif
