#include "operation.h"

// turn_left ・ turn_right　で使用
static float op_pd;

static float count_R, count_L;		//回転角
// static float DISTANCE = WIDTH * M_PI  / 4;	//カーブに必要な距離

//カラーセンサーで取得した値を格納
// static S16 rgb[3];
S16 rgb[3];
static int color;
static int ignore_count = 0; //誤検知対策 停止ループに入っても無視するカウンタ

//関数名: decide_color
//引数: 無し
//戻り値: 無し
//概要: 回収した玉は赤か青か、もしくは失敗したかを判断して以後の行動を決める
int decide_color(){

	// int counter; //ループカウンター変数
	// int r_val=0, b_val=0; //赤・青値を格納
	int color_avg=0; //赤と青の平均を格納
	int bunsan=0; //RGB分散値を格納
	int ball;			//ボールの色 0:回収失敗, 1:赤, 2:青

	//こまま二乗すると値があふれてしまうので、1000で割る
	//下3桁切り捨ててもRGBの差が確認できるはずである
	r_val /= 1000;
	b_val /= 1000;
	color_avg = (r_val + b_val) / 2; //赤青の平均を取得

	//分散値を計算 (色値-赤青値平均)^2 ÷ 2色
	bunsan = (r_val - color_avg) * (r_val - color_avg) + (b_val - color_avg) * (b_val - color_avg);
	bunsan /= 2;

	if( bunsan > 3 ){ //値に散らばりがあればボールを持っている
		if(r_val > b_val){ //赤の値が大きければ赤いボールを持っている。 それ以外は青いボールを持っている。
			ball = 1; //赤
		}else{
			ball = 2; //青
		}
	}else{	//値に散らばりがなければ回収失敗
		ball = 0; //回収失敗
	}

	//curves[]を extern static intにして書き換える
	display_goto_xy(0, 2);
	display_string("R:");
	display_int(r_val,0);

	display_goto_xy(0, 3);
	display_string("B:");
	display_int(b_val,0);

	display_goto_xy(0, 4);
	display_string("bunsan: ");
	display_int(bunsan,0);

	display_goto_xy(0, 5);
	display_string("my ball is: ");
	display_int(ball,0);

	display_update();
	//ecrobot_bt_data_logger(diff, rgb[0]);

	systick_wait_ms(4);

	return ball;
}
//180°ターン

//ショートカット処理
void shortcut(){

	static float in = 80, out = 90;
	//ogain が大きいほど,igain が小さいほど　小さい円を描く
	static float  adj = 0, ogain = 3.0, igain = 2.12;//スピード調整用
	int flag = 0;		//左右判断用
	float curv_value = (60 + (7.9 * 2)) * M_PI / 2 * C1;		//直径60cm+センサーから外輪*2 の円周の半分(60,7.6)

	while(flag < 4){
		count_reset(1,1);
		switch (flag) {
			case 0:		//右にカーブ
			case 2:
				while(count_L <= curv_value + C1 * 2){		//距離で弧の長さを調整
					nxt_motor_set_speed(R_MOTOR,in - adj,1);
					nxt_motor_set_speed(L_MOTOR,out + adj,1);
					adj = (count_R * ogain) - (count_L * igain);		 //速度で弧の長さを調整
					if(adj > 20){
						adj = 20;
					}
					count_L = nxt_motor_get_count(L_MOTOR);
					count_R = nxt_motor_get_count(R_MOTOR);
				}
				break;

			case 1:	//左にカーブ
				while(count_R <= curv_value + C1){
					nxt_motor_set_speed(L_MOTOR,in - adj,1);
					nxt_motor_set_speed(R_MOTOR,out + adj,1);
					adj = (count_L * ogain) - (count_R * igain);
					if(adj > 20){
						adj = 20;
					}
					count_L = nxt_motor_get_count(L_MOTOR);
					count_R = nxt_motor_get_count(R_MOTOR);
				}
				break;

			case 3:		//最後のカーブ
				while(count_R <= (curv_value - (C1 * 6))){		//弧の長さを調整
					nxt_motor_set_speed(L_MOTOR,in - adj,1);
					nxt_motor_set_speed(R_MOTOR,out + adj,1);
					adj = (count_L * ogain) - (count_R * (igain - 0.01));		//弧を描くための円の大きさを調整
					if(adj > 20){
						adj = 20;
					}
					count_L = nxt_motor_get_count(L_MOTOR);
					count_R = nxt_motor_get_count(R_MOTOR);
				}
				break;

				default:
				break;
		}
		flag++;
	}
}

// read_mode
//	*0: 右でラインを読み 左で交差点を検知
//	*1: 左でラインを読み 右で交差点を検知
void return_line(int read_mode){
	switch (read_mode) {
		case 0:		//次が右で読む場合
			while(ecrobot_get_light_sensor(R_LIGHT_SENSOR) < GRAY_VAL - 25){		//少し弱めに曲がる
				nxt_motor_set_speed(R_MOTOR,45,1);	//55
				nxt_motor_set_speed(L_MOTOR,42,1);	//50
			}
			systick_wait_ms(5);
			count_reset(0,0);
			while(nxt_motor_get_count(R_MOTOR) < C1 * 6){		//カーブ誤検知対策に、少し安定させる
				op_pd = read_right();
				run_right_sensor(38,op_pd);		//遅いほうが安定する
			}
			break;

		case 1:		//次が左で読む場合
			while(ecrobot_get_light_sensor(L_LIGHT_SENSOR) < GRAY_VAL -25){		//少し弱めに方向転換
				nxt_motor_set_speed(R_MOTOR,42,1);	//50
				nxt_motor_set_speed(L_MOTOR,45,1);	//55
			}
			systick_wait_ms(5);
			count_reset(0,0);
			while(nxt_motor_get_count(L_MOTOR) < C1 * 6){		//誤検知対策
				op_pd = read_left();
				run_left_sensor(38,op_pd);		//遅いほうが安定
			}
			break;
	}
}

//ボールの収納
//引数 : ボールの色 color
void receive(int color){
	count_reset(0,0);
	switch (color) {
		case 1:		//赤の時左折
			while(nxt_motor_get_count(R_MOTOR) < C1 * DISTANCE - 5){	//方向転換
				nxt_motor_set_speed(L_MOTOR,-50,1);	//50
				nxt_motor_set_speed(R_MOTOR,50,1);	//-50
				systick_wait_ms(3);
			}
			count_reset(0,0);
			while(nxt_motor_get_count(R_MOTOR) < C1 * 12){		//ラインを読みながら少し進める
				op_pd = read_left();
				run_left_sensor(50,op_pd);
			}
			stop(10);
			arm_open();
			move_ahead(100,C1 * 4);	//ボールを押し出す
			stop(300);		//ボールがアームに当たらないように待つ
			count_reset(0,0);
			while(nxt_motor_get_count(L_MOTOR) < C1 * DISTANCE + 15 ){		//ゴールのほうを向く
				nxt_motor_set_speed(L_MOTOR,50,1);
				nxt_motor_set_speed(R_MOTOR,-50,1);
				systick_wait_ms(3);
			}
			break;

		case 2:		//青のとき右折
			while(nxt_motor_get_count(L_MOTOR) < (C1 * DISTANCE)){	//車幅を直径とする円周の4分の1
				nxt_motor_set_speed(L_MOTOR,50,1);	//51
				nxt_motor_set_speed(R_MOTOR,-50,1);	//-50
				systick_wait_ms(3);
			}
			count_reset(0,0);
			while(nxt_motor_get_count(R_MOTOR) < C1 * 12){
				op_pd = read_right();
				run_right_sensor(50,op_pd);
			}
			stop(10);
			arm_open();
			move_ahead(100,C1 * 4);	//ボールを押し出す
			stop(300);
			count_reset(0,0);
			while(nxt_motor_get_count(R_MOTOR) < C1 * DISTANCE + 15 ){
				nxt_motor_set_speed(R_MOTOR,50,1);
				nxt_motor_set_speed(L_MOTOR,-50,1);
				systick_wait_ms(3);
			}
			break;

		default:
			break;
	}
}

//超音波センサーを使い、止まる
void starrySky(){		//SHORI NO OWSRI

	int dist_wall; //壁までの距離

	dist_wall = ecrobot_get_sonar_sensor(SONAR);      /* 超音波センサから値を取得 */
	if(dist_wall <= 40 ){
		if(ignore_count > 2 ){
			display_clear(0);
			display_goto_xy(0, 1);
			display_string("SHORI NO OWARI");
			display_update();
			 while(1){
				 run_right_sensor(0,0);
				 play_music(sheet_RPG, RPG_LENGTH);
			 }
		 } //停止無限ループ
		ignore_count++;
	}
	systick_wait_ms(60);		//50以上は必要
}

//ボールを入れた後、黒を読んだら反対に少し曲がるようにする
//引数 : ボールの色
void doraGenai(int congratulation){

	int fireBird = 0;
	int speed_R = 50, speed_L = 50;

	while(1){
		if(fireBird == 0 &&
				(ecrobot_get_light_sensor(R_LIGHT_SENSOR) > GRAY_VAL || ecrobot_get_light_sensor(L_LIGHT_SENSOR) > GRAY_VAL)){		//speedの値が変わってない時だけ
			fireBird++;
			switch (congratulation) {
				case 1:
					speed_R = 70;
					break;

				case 2:
					speed_L = 70;
					break;

				default:
						break;
			}
		}
		nxt_motor_set_speed(R_MOTOR,speed_R,1);
		nxt_motor_set_speed(L_MOTOR,speed_L,1);
		starrySky();
	}
}

//*****************************************************************************
// 関数名 : decide_control
// 第1引数 op: 左右、直進、旋回の命令
//    0: 左旋回
//    1: 右旋回
//    2: 直進
//    3: 180旋回
// 第2引数 read_mode: 旋回後どちらのセンサーで線を読むか渡す
//    0: 右読み取り
//    1: 左読み取り
// 戻り値 : なし
// 概要 : 何するか決める
//*****************************************************************************
void decide_control(int op, int read_mode){
	switch(op){

    case 0:
			turn_left(read_mode);
		break;

		case 1:
			turn_right(read_mode);
		break;

		case 2:
		break;

    case 3:
			turn(read_mode);
			return_line(0);
		break;

		case 4:
			shortcut();
			return_line(read_mode);
		break;

		case 5:
			turn_left(read_mode);
			return_line(read_mode);
			count_reset(0,0);
			while(nxt_motor_get_count(R_MOTOR) < C1 * 70){
				op_pd = read_gray();
				run_right_sensor(50,op_pd);
			}
			color = decide_color();
			break;

		case 6:
			receive(color);
			stop(100);
			doraGenai(color);
		break;

    default:
			break;
	}
}
