#include "etc.h"

//モーターの回転角初期化
//引数 : (左回転角,右回転角)
void count_reset(int value_L, int value_R){
	nxt_motor_set_count(L_MOTOR,value_L);
	nxt_motor_set_count(R_MOTOR,value_R);
}

//止まる
//引数 : 止めるミリ秒
void stop(int ms){
	nxt_motor_set_speed(R_MOTOR,0,1);
	nxt_motor_set_speed(L_MOTOR,0,1);
	systick_wait_ms(ms);
}
//指定した速度で指定した距離 (引数 回転角) 進ませる
//speed : 速さ, angle : 回転角
void move_ahead(int speed, int angle){

	count_reset(0,0);
	while(nxt_motor_get_count(L_MOTOR) <= angle){
		nxt_motor_set_speed(L_MOTOR,speed,1);
		nxt_motor_set_speed(R_MOTOR,speed,1);
		systick_wait_ms(5);
	}
}
