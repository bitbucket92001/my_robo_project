#ifndef _UTIL
#define _UTIL

#include "ecrobot_interface.h"
#include "constant.h"
#include "nxt_init.h"

extern void count_reset(int , int);
extern void stop(int);
extern void move_ahead(int , int);

#endif
