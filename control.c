#include "control.h"

//PD制御の操作量が入る変数
static int op_pd;

//pd制御の操作量を求めるために
static int light;
static int light_tmp = GRAY_VAL;

void run_right_sensor(int speed,int turn){
	nxt_motor_set_speed(L_MOTOR,speed+turn,1);
	nxt_motor_set_speed(R_MOTOR,speed-turn,1);
//	ecrobot_bt_data_logger(0, 0);
	systick_wait_ms(4);
}

void run_left_sensor(int speed,int turn){
	nxt_motor_set_speed(L_MOTOR,speed-turn,1);
	nxt_motor_set_speed(R_MOTOR,speed+turn,1);
//	ecrobot_bt_data_logger(0, 0);
	systick_wait_ms(4);
}

void run_back(int speed,int turn){
	nxt_motor_set_speed(L_MOTOR,-speed+turn,1);
	nxt_motor_set_speed(R_MOTOR,-speed-turn,1);
	systick_wait_ms(4);
}

float read_right(){
	float op_pd;
	light = ecrobot_get_light_sensor(R_LIGHT_SENSOR);
	op_pd = P_GAIN * (light - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) + D_GAIN * (light-light_tmp)/(float)(BLACK_VAL - WHITE_VAL )*100;
	light_tmp=light;
	return op_pd;
//	ecrobot_bt_data_logger(0, 0);
//	systick_wait_ms(4);
}

float read_left(){
	float op_pd;
	light = ecrobot_get_light_sensor(L_LIGHT_SENSOR) + GOSA;
	op_pd = P_GAIN * (light - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) + D_GAIN * (light-light_tmp)/(float)(BLACK_VAL - WHITE_VAL )*100;
	light_tmp=light;
	return op_pd;
//	ecrobot_bt_data_logger(0, 0);
//	systick_wait_ms(4);
}
float read_gray(){
	light = ecrobot_get_light_sensor(R_LIGHT_SENSOR);
	if(ecrobot_get_light_sensor(R_LIGHT_SENSOR) > GRAY_VAL){
		light = GRAY_VAL;
	}
	op_pd = P_GAIN * (light - ((GRAY_VAL + WHITE_VAL) / 2))*100 / (GRAY_VAL - WHITE_VAL) + 8 * (light-light_tmp)/(float)(GRAY_VAL - WHITE_VAL )*100;
	light_tmp=light;
	return op_pd;
}
