#ifndef _TURN_H
#define _TURN_H

#include "ecrobot_interface.h"
#include "constant.h"
#include "nxt_init.h"
#include "operation.h"
#include "control.h"
#include "etc.h"

extern void turn(int);
extern void turn_left(int);
extern void turn_right(int);

#endif
