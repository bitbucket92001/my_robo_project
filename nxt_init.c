#include "constant.h"
#include "nxt_init.h"

void ecrobot_device_initialize(){		/* OSEK起動時の処理 */
	ecrobot_init_bt_slave("LEJOS-OSEK");
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	nxt_motor_set_speed(ARM_MOTOR,0,0);
	ecrobot_set_light_sensor_active(L_LIGHT_SENSOR);
	ecrobot_set_light_sensor_active(R_LIGHT_SENSOR);
	ecrobot_init_nxtcolorsensor(COLOR_SENSOR, NXT_COLORSENSOR);
	ecrobot_init_sonar_sensor(SONAR);  /* 超音波センサの初期化/計測開始 */
}

void ecrobot_device_terminate(){		/* OSEK終了時の処理 */
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	nxt_motor_set_speed(ARM_MOTOR,0,0);
	ecrobot_set_light_sensor_inactive(L_LIGHT_SENSOR);
	ecrobot_set_light_sensor_inactive(R_LIGHT_SENSOR);
	ecrobot_term_nxtcolorsensor(COLOR_SENSOR);
	ecrobot_term_bt_connection();
	ecrobot_term_sonar_sensor(SONAR); /* 超音波センサ計測を終了 */
}

void user_1ms_isr_type2(void){}
