# Target specific macros
TARGET = PDcontrol_bot

# 自分のフォルダから見たnxtOSEKフォルダの場所またはそのフルパスを指定するここでは次のフルパスを指定する。
NXTOSEK_ROOT = /nxtOSEK

# User application source（ここにソースファイルの名前を指定する）
TARGET_SOURCES := \
	nxt_init.c \
	arm.c \
	control.c \
	etc.c \
	music.c \
	turn.c \
	operation.c \
	main.c

# OSEK OIL file（ここにoilファイルの名前を指定する）
TOPPERS_OSEK_OIL_SOURCE := ./main.oil


# below part should not be modified
#ここは自分の環境に合わせて書き直す
O_PATH ?= build
include ../../ecrobot/ecrobot.mak
